# Bool

[Bool](http://crystal-lang.org/api/Bool.html) は真偽値を表すもので、`true` と `false` という2つだけの値が存在します。以下のようなリテラルで利用できます。


```ruby
true  # 真 である Bool
false # 偽 である Bool
```
